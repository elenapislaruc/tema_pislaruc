function addTokens(input, tokens) {

    if (typeof input != "string") {
        throw new Error("Invalid input");
    }
    if (input.length < 6) {
        throw new Error("Input should have at least 6 characters")
    }

    var i = 0;
    for (; i < tokens.length; i++) {
        if (typeof tokens[i].tokenName != "string") {
            throw new Error("Invalid array format");
        }
    }
    if (input.includes("...") == false) {
        return input;
    }
    i = 0;
    var result;
    for (; i < tokens.length; i++) {
       result = input.replace("...", "${" + tokens[i].tokenName + "}");
    }
    return result;
}

const app = {
    addTokens: addTokens
}

module.exports = app;